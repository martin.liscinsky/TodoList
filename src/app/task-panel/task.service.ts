import { Task } from './task';

export class TaskService {
	private tasks: Task[] = [];
	private selectedId: number = -1;
  private editMode: boolean = false;

  public getTasks() {
  	return this.tasks;
  }

  public setTasks(tasks: Task[]) {
    this.tasks = tasks;
  }

  public getSelected() {
  	return this.selectedId;
  }

  public switchEditMode() {
    this.editMode = !this.editMode;
  }

  public getEditMode() {
    return this.editMode;
  }

  public addTask() {
  	this.tasks.push(new Task('Task name', 'Notes'));
  	this.selectedId = this.tasks.length - 1;
    this.editMode = true;
  }

  public selectTask(selectedId: number) {
  	this.selectedId = selectedId;
  }

  public deleteSelectedTask() {
  	this.tasks.splice(this.selectedId, 1);
  	this.selectedId = -1;
    this.editMode = false;
  }

  public selectedUp() {
    if (this.selectedId > 0) {
      let tmp = this.tasks[this.selectedId - 1];
      this.tasks[this.selectedId - 1] = this.tasks[this.selectedId];
      this.tasks[this.selectedId] = tmp;
      --this.selectedId;
    }
  }

  public selectedDown() {
    if (this.selectedId < this.tasks.length - 1) {
      let tmp = this.tasks[this.selectedId + 1];
      this.tasks[this.selectedId + 1] = this.tasks[this.selectedId];
      this.tasks[this.selectedId] = tmp;
      ++this.selectedId;
    }
  }

}
