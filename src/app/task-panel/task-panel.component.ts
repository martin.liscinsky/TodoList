import { Component, OnInit } from '@angular/core';

import { Task } from './task';
import { TaskService } from './task.service';

@Component({
  selector: 'tl-task-panel',
  templateUrl: './task-panel.component.html'
})
export class TaskPanelComponent implements OnInit {
	tasks: Task[];

  constructor(private taskService: TaskService) {

  }

  ngOnInit() {
  	this.tasks = this.taskService.getTasks();
  }

  newTask() {
  	this.taskService.addTask();
  }

}
