import { Component, Input, DoCheck } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Task } from '../task';
import { TaskService } from '../task.service';

@Component({
  selector: 'tl-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})

export class TaskItemComponent implements DoCheck {
  @Input() taskId: number;
  @Input() task: Task;
  activeSwitch = false;
  editMode = false;
  completeClass = '';

  constructor(private taskService: TaskService) {

  }

  onClick(event) {
    if (event.target.tagName !== 'BUTTON') {
      if (this.activeSwitch) {
        this.taskService.selectTask(-1);
      } else {
        this.taskService.selectTask(this.taskId);
      }
      this.activeSwitch = (this.taskService.getSelected() === this.taskId);
    }
  }

  editClick() {
    this.taskService.switchEditMode();
  }

  deleteClick() {
    this.taskService.deleteSelectedTask();
  }

  onSubmit(form: NgForm) {
    this.task.name = form.value.taskName;
    this.task.description = form.value.taskDescription;
    this.taskService.switchEditMode();
  }

  cancelEdit() {
    this.taskService.switchEditMode();
  }

  switchCompletion() {
    this.task.completed = !this.task.completed;
  }

  swapUp() {
    this.taskService.selectedUp();
  }

  swapDown() {
    this.taskService.selectedDown();
  }

  ngDoCheck() {
    this.completeClass = this.task.completed ? 'compl' : '';
    this.activeSwitch = (this.taskService.getSelected() === this.taskId);
    if (this.activeSwitch) {
      this.editMode = this.taskService.getEditMode();
    } else {
    this.editMode = false;
    }
  }

}
