export class Task {
  public completed = false;
  constructor(public name: string, public description: string) {
  }
}
