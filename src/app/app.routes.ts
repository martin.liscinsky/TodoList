import { RouterModule, Routes } from '@angular/router';

import { TaskPanelComponent } from './task-panel/task-panel.component';
import { SavePanelComponent } from './save-panel/save-panel.component';
import { LoadPanelComponent } from './load-panel/load-panel.component';

const APP_ROUTES: Routes = [
		{ path: '', redirectTo: '/tasks', pathMatch: 'full' },
		{ path: 'tasks', component: TaskPanelComponent },
		{ path: 'save', component: SavePanelComponent },
		{ path: 'load', component: LoadPanelComponent },
		{ path: '**', redirectTo: '/tasks' }
];

export const routing = RouterModule.forRoot(APP_ROUTES);