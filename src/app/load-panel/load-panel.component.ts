import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { TaskService } from '../task-panel/task.service';
import { Task } from '../task-panel/task';

@Component({
  selector: 'tl-load-panel',
  templateUrl: './load-panel.component.html'
})
export class LoadPanelComponent {
	
  constructor(private taskService: TaskService) { }

  onSubmit(form: NgForm) {
  	let store = localStorage.getItem(form.value.loadName);
  	if (store != null) {
			this.taskService.setTasks(JSON.parse(store));
  	}
  }

}
