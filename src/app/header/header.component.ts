import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'tl-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  constructor(private router: Router) {
  }

  toList() {
    this.router.navigateByUrl('/tasks');
  }
  toSave() {
    this.router.navigateByUrl('/save');
  }
  toLoad() {
    this.router.navigateByUrl('/load');
  }
}
