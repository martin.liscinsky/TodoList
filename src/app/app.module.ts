import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdSidenavModule, MdButtonModule, MdCardModule, MdInputModule, MdToolbarModule } from '@angular/material';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { TaskPanelComponent } from './task-panel/task-panel.component';
import { TaskItemComponent } from './task-panel/task-item/task-item.component';
import { routing } from './app.routes';
import { TaskService } from './task-panel/task.service';
import { SavePanelComponent } from './save-panel/save-panel.component';
import { LoadPanelComponent } from './load-panel/load-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TaskPanelComponent,
    TaskItemComponent,
    SavePanelComponent,
    LoadPanelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    BrowserAnimationsModule,
    MdSidenavModule,
    MdButtonModule,
    MdCardModule,
    MdInputModule,
    MdToolbarModule
  ],
  providers: [TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
