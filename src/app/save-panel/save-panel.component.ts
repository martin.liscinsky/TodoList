import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { TaskService } from '../task-panel/task.service';

@Component({
  selector: 'tl-save-panel',
  templateUrl: './save-panel.component.html'
})
export class SavePanelComponent {

  constructor(private taskService: TaskService) { }

  onSubmit(form: NgForm) {
  	localStorage.setItem(form.value.saveName, JSON.stringify(this.taskService.getTasks()));
  }
  
}
